<?php
/**
 * 绑定用户信息
 * [WeEngine System] Copyright (c) 2014 W7.CC.
 */
defined('IN_IA') or exit('Access Denied');
load()->model('user');

$dos = array('display', 'validate_mobile', 'bind_mobile', 'bind_oauth', 'console', 'bind-console', 'verify_mobile', 'send', 'bind_console', 'console_url');
$do = in_array($do, $dos) ? $do : 'display';

if (in_array($do, array('validate_mobile', 'bind_mobile', 'verify_mobile', 'send', 'bind_console'))) {
	$user_profile = table('users_profile')->getByUid($_W['uid']);
	$mobile = safe_gpc_string($_GPC['mobile'], '', 'mobile');
	if (empty($mobile)) {
		iajax(-1, '手机号不能为空');
	}
	if (!preg_match(REGULAR_MOBILE, $mobile)) {
		iajax(-1, '手机号格式不正确');
	}
	$bind_type = in_array($do, array('verify_mobile', 'send', 'bind_console')) ? USER_REGISTER_TYPE_CONSOLE : USER_REGISTER_TYPE_MOBILE;
	$mobile_exists = table('users_bind')->getByTypeAndBindsign($bind_type, $mobile);
	if (empty($type) && !empty($mobile_exists)) {
		iajax(-1, '手机号已存在');
	}
}

if ('validate_mobile' == $do) {
	iajax(0, '本地校验成功');
}

if ('bind_mobile' == $do) {
	if ($_W['isajax'] && $_W['ispost']) {
		$bind_info = OAuth2Client::create('mobile')->bind();
		if (is_error($bind_info)) {
			iajax(-1, $bind_info['message']);
		}
		iajax(0, '绑定成功', url('user/profile/bind'));
	} else {
		iajax(-1, '非法请求');
	}
}

if ('display' == $do) {
	$support_bind_urls = user_support_urls();
	$setting_sms_sign = setting_load('site_sms_sign');
	$bind_sign = !empty($setting_sms_sign['site_sms_sign']['register']) ? $setting_sms_sign['site_sms_sign']['register'] : '';
	if (!empty($_W['user']['type']) && $_W['user']['type'] == USER_TYPE_CLERK) {
		$_W['setting']['copyright']['bind'] = empty($_W['setting']['copyright']['clerk']['bind']) ? '' : $_W['setting']['copyright']['clerk']['bind'];
	}
	if (empty($_W['isw7_request']) && !empty($_W['setting']['copyright']['console_status'])) {
		$_W['setting']['copyright']['bind'] = 'console';
	}
}

if ('bind_oauth' == $do) {
	$uid = intval($_GPC['uid']);
	$openid = safe_gpc_string($_GPC['openid']);
	$register_type = intval($_GPC['register_type']);

	if (empty($uid) || empty($openid) || !in_array($register_type, array(USER_REGISTER_TYPE_QQ, USER_REGISTER_TYPE_WECHAT))) {
		itoast('参数错误!', url('user/login'), '');
	}
	$user_info = user_single($uid);
	if ($user_info['is_bind']) {
		itoast('账号已绑定!', url('user/login'), '');
	}

	if ($_W['ispost']) {
		$member['username'] = safe_gpc_string($_GPC['username']);
		$member['password'] = safe_check_password($_GPC['password']);
		$member['repassword'] = safe_check_password($_GPC['repassword']);
		$member['is_bind'] = 1;

		if (empty($member['username']) || empty($member['password']) || empty($member['repassword'])) {
			itoast('请填写完整信息！', referer(), '');
		}
		if (!preg_match(REGULAR_USERNAME, $member['username'])) {
			itoast('必须输入用户名，格式为 3-15 位字符，可以包括汉字、字母（不区分大小写）、数字、下划线和句点。', referer(), '');
		}
		if (user_check(array('username' => $member['username']))) {
			itoast('非常抱歉，此用户名已经被注册，你需要更换注册名称！', referer(), '');
		}
		if (is_error($member['password'])) {
			itoast($member['password']['message'], referer(), '');
		}
		if ($member['password'] != $member['repassword']) {
			itoast('两次秘密输入不一致');
		}
		unset($member['repassword']);
		if (user_check(array('username' => $member['username']))) {
			itoast('非常抱歉，此用户名已经被注册，你需要更换注册名称！', referer(), '');
		}

		$member['salt'] = random(8);
		$member['password'] = user_hash($member['password'], $member['salt']);
		$result = pdo_update('users', $member, array('uid' => $uid, 'openid' => $openid, 'register_type' => $register_type));
		if ($result) {
			itoast('注册绑定成功!', url('user/login'), '');
		} else {
			itoast('注册绑定失败, 请联系管理员解决!', url('user/login'), '');
		}
	} else {
		template('user/bind-oauth');
		exit;
	}
}

if ('console' == $do) {
	template('user/console');
	exit();
}

if ('bind-console' == $do) {
	template('user/bind-console');
	exit();
}

if (in_array($do, array('verify_mobile', 'send', 'bind_console'))) {
	if (!$_W['isajax']) {
		iajax(-1, '非法请求');
	}
}

if ('verify_mobile' == $do) {
	$code = safe_gpc_string($_GPC['code']);
	if (empty($code)) {
		iajax(-1, '请输入验证码');
	}
	$data = array(
		'mobile' => $mobile,
		'sms_verify_code' => $code
	);
	$result = cloud_check_mobile($data);
	if (is_error($result)) {
		iajax(-1, $result['message']);
	}
	if (!empty($result['out_user_id'])) {
		if ($_W['uid'] != $result['out_user_id']) {
			$user = pdo_get('users', array('uid' => $result['out_user_id']), array('username'));
			$message = '该手机号已绑定用户 ' . empty($user['username']) ? '' : $user['username'] . ' ，请更换其他手机号';
			iajax(-1, $message);
		}
	}
	iajax(0, $result);
}

if ('send' == $do) {
	$result = cloud_send_code($mobile);
	if (is_error($result)) {
		iajax(-1, $result['message']);
	}
	iajax(0, '验证码发送成功');
}

if ('bind_console' == $do) {
	$token = safe_gpc_string($_GPC['token']);
	$username = safe_gpc_string($_GPC['username']);
	$type = safe_gpc_int($_GPC['type']);
	if (in_array($_W['uid'], explode(',', $_W['config']['setting']['founder']))) {
		iajax(-1, '创始人不可以绑定控制台');
	}
	if (empty($type)) {
		if (empty($token)) {
			iajax(-1, '参数错误');
		}
		$data = array(
			'user_name' => empty($username) ? $mobile : $username,
			'mobile' => $mobile,
			'token' => $token,
			'out_user_id' => $_W['uid'],
		);
		$result = cloud_bind_user($data);
		if (is_error($result)) {
			iajax(-1, $result['message']);
		}
	}
	$user_bind = pdo_get('users_bind', array('uid' => $_W['uid'], 'third_type' => USER_REGISTER_TYPE_CONSOLE));
	if (empty($user_bind)) {
		pdo_insert('users_bind', array('uid' => $_W['uid'], 'bind_sign' => $mobile, 'third_type' => USER_REGISTER_TYPE_CONSOLE, 'third_nickname' => $mobile));
	} else {
		pdo_update('users_bind', array('bind_sign' => $mobile, 'third_nickname' => $mobile), array('id' => $user_bind['id']));
	}
	iajax(0, '绑定成功');
}

if ('console_url' == $do) {
	$type = safe_gpc_string($_GPC['type']);
	if (empty($type) || !in_array($type, array('we7', 'client', 'system'))) {
		itoast('参数错误！', url('user/third-bind/console'), 'info');
	}
	if ('system' == $type) {
		$settings = $_W['setting']['copyright'];
		$settings['console_status'] = intval(!$settings['console_status']);
		$visible = !empty($settings['console_status']) ? STATUS_ON : STATUS_OFF;
		$result = cloud_verify_console_visible($visible);
		if (is_error($result)) {
			itoast($result['message']);
		}
		setting_save($settings, 'copyright');
		if (!$visible) {
			header('location: ' . $_W['siteroot'] . 'web/home.php');
			exit();
		} else {
			$type = 'we7';
		}
	}
	$data = [
		'out_user_id' => empty($_W['isadmin']) ? $_W['uid'] : 0,
		'console_type' => $type
	];
	$result = cloud_console_index_url($data);
	if (is_error($result)) {
		itoast($result['message'], url('user/third-bind/console'), 'info');
	}
	isetcookie('__direct_to_console', 1);
	$url = $result['data'];
	header('location: ' . $url);
	exit();
}
template('user/third-bind');
