<?php

namespace We7\V220;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1595316458
 * @version 2.2.0
 */

class CreateUndocodeauditLogTable {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('wxapp_undocodeaudit_log')) {
			$table_name = tablename('wxapp_undocodeaudit_log');
			$sql = <<<EOF
CREATE TABLE IF NOT EXISTS $table_name (
`id` int(11) unsigned not null AUTO_INCREMENT,
`uniacid` int(11) not null COMMENT '所属账号uniacid',
`version_id` int(10) not null COMMENT '小程序版本ID',
`auditid` int(11) not null COMMENT '审核ID',
`revoke_time` int(11) not null COMMENT '审核撤回时间',
PRIMARY KEY(`id`),
KEY `uniacid` (`uniacid`),
KEY `version_id` (`version_id`),
KEY `auditid` (`auditid`)
) DEFAULT CHARSET=utf8 COMMENT='授权小程序版本审核撤销记录表';
EOF;
			pdo_query($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
